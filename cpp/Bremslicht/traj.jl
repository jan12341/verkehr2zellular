#!/usr/bin/julia

using Plots
using FileIO
using DelimitedFiles


m=readdlm(ARGS[1]);
plot=Plots.heatmap(m,legend=false,c=ColorGradient([:white,:black]),xlabel="Ort [Zellen]",ylabel="Zeit [Schritte]")
name = string(ARGS[1], ".png")
save(name,plot)



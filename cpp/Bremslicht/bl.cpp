#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>

#define loop(x, n) for(int x = 0; x < n; ++ x)

using namespace std;

// Definieren einiger Konstanten
#define sl 1000 //10000
#define maxv 5
//#define trw 0.3
#define trwanf 0.5
#define trwb 0.94
#define trwd 0.1 //0,1 vorher
#define eqz 2000
#define mez 6000 //6000 //muss vielfaches von 60 sein, am besten so, dass mez / (60*20) eine ganze Zahl ist
#define anzd 100 // mw.jl arbeitet mit 100 dichten
#define dsafe 4 // oder 2 //Sicherheitsabstand nicht größer als maxv
#define h 7 //Blickweite

// Deklaierung und Erzeugung des Structs für alle Autoinfos und des Gitters
struct autoinfos {
    bool a;
    int v;
    bool b;
    bool md;
}; 

struct autoinfos s[sl];  // array aus den structs

struct autoinfos  leer = {false, 0, false, false};

#ifdef EINS
double flussdichte [anzd+1][2];
#endif

#ifdef MITTEL
double flussdichte [anzd+1][1+mez/60];
#endif

#ifdef TRAJ
int strecke [mez][sl];
#endif

//Funktion zur zufälligen Besetzung
bool randomBool()
{
    return (rand() % 2) == 1;
}

//Funktion für Trödeln
double trödeln()
{
    return ((double) rand()/ RAND_MAX);
}

//Funktion für Anfangsgeschwindigkeit
int zufallszahl(int obere_grenze)
{
    return rand()/double(RAND_MAX)*(obere_grenze+1);
}

//Funktionen, die für Bremslichtmodell benötigt werden


//Funktion die für alle Autos die Updates macht
void update(int zeit,int dichtezahl){
	int vant[sl]; //Beschreibungen siehe Berechnung
	int dadd[sl];
	int deff[sl];
	double dtime[sl];
	int dint[sl];
	int bm[sl];
	double trw[sl];
    int lücke[sl];
    // Schritt 0, wichtige Parameter, die im Folgenden benötigt werden, berechnen
	loop(i,sl){
        if (s[i].a==true) {
            int j = (i+1) % sl; //+1 weil wir erst die naechste Zelle nehmen
            int lucke = 0;
            while (s[j].a == false){ //wir gucken nicht mehr nur so weit wie unsere Geschwindigkeit, weil wir die Position des Vordermannes benötigen
                lucke++;
                j = ( j + 1) % sl; //verwende periodische RB
            }
            lücke[i]=lucke;
			vant[i] = lucke < s[i].v ? lucke - 1 : s[i].v - 1; //Antizipierte Geschwindigkeit des Vordermannes, siehe Skript
        }
    }
    loop(i,sl){
        if(s[i].a==true){
            int j = (i+lücke[i]+1)%sl;
            dadd[i] = vant[j] - dsafe > 0 ? vant[j] - dsafe : 0; //Abstand der zum Abstand addiert wird um den effektiven Abstand zu erhalten
			deff[i] = lücke[i] + dadd[i]; //Effektiver Abstand zum Vordermann
			dtime[i] = double(lücke[i]) /s[i].v; //Zeitlicher Abstand zum Vordermann
			dint[i] = s[i].v < h ? s[i].v : h; //Interaktionshorizont
            bm[i] = s[j].b == true ? 1: 0;
			if (s[i].v == 0) { 
				trw[i] = trwanf;
			} else if(s[j].b == true && dtime[i] < dint[i]) {
				trw[i] = trwb;
			} else {
				trw[i] = trwd;
			}
        }
    }	
	
    // Schritt 1, Beschleunigen, falls noch nicht Maximalgeschwindigkeit
    loop(i,sl){
        if (s[i].a == true) {
			if (s[i].b == true || (bm[i] == 1 && dtime[i] < dint[i]) ) {
                s[i].v = s[i].v ;
			} else {
				s[i].v = s[i].v < maxv ? s[i].v + 1 : maxv; // alle ternären Operatoren durch min(a,b) ersetzen
			}
          s[i].b = false;
		}
    }
    /*int anz=0;
    loop(i,sl) if(s[i].a==true) anz++;
    cout << "@1 \t" << anz << "\n"; */
    
    // Schritt 2, falls Lücke kleiner als Geschwindigkeit -> Geschwindigkeit = Lücke, sonst nichts
    loop(i,sl){
		//int vschritt[sl] = 0; //Wir benötigen eine Geschwindigkeit zum zwischenspeichern in diesem Schritt
        if (s[i].a==true) {
			int vschritt = s[i].v < deff[i] ? s[i].v : deff[i]; //Wir benötigen eine Geschwindigkeit zum zwischenspeichern in diesem Schritt
            if (vschritt < s[i].v) {s[i].b = true; }
            s[i].v = vschritt;
        }
    }
    /*anz=0;
    loop(i,sl) if(s[i].a==true) anz++;
    cout << "@2 \t" << anz << "\n"; */
    
    // Schritt 3, Trödeln: Verringerung mit Wahrscheinlichkeit p, wenn nicht schon steht
    loop(i,sl){
        if (s[i].a == true && s[i].v != 0){
			int vschritt = trödeln() < trw[i] ? s[i].v -1 : s[i].v; //Wir benötigen eine Geschwindigkeit zum zwischenspeichern in diesem Schritt
            if (vschritt < s[i].v && trw[i] == trwb) {s[i].b = true; }
			s[i].v = vschritt;
        }
    }
    /*anz=0;
    loop(i,sl) if(s[i].a==true) anz++;
    cout << "@3 \t" << anz << "\n";*/
   
    // Schritt 4, alle mit Geschwindigkeit nach vorne
    
    struct autoinfos autozwischen[sl];
    
    loop(i,sl){
        if (s[i].a == true && s[i].md == false){
            int neuerplatz = (i + s[i].v) % sl; //periodische Randbedingungen
           
            if (zeit > eqz && neuerplatz < i){ // wenn equilibriert und auto rechts raus
#ifdef MITTEL
                 int step = zeit / 60 + 1; // gibt die größte zahl die durch 60 geht
                 flussdichte[dichtezahl][step]++; //fluss einen hoch
#endif
#ifdef EINS
                flussdichte[dichtezahl][1]++;
#endif
            }
            
			autozwischen[neuerplatz] = s[i];
            s[i].md = true;
			// if (s[neuerplatz].v > 0) s[i] = leer;
        }
    }
    loop(i,sl) {
        s[i] = autozwischen[i];
        autozwischen[i]=leer;
    }
    /*anz=0;
    loop(i,sl) if(s[i].a==true) anz++;
    cout << "@4 \t" << anz << "\n";*/
    
    loop(i,sl){ s[i].md = false;}
}



int main(int argc, char* argv[]){

    srand(time(0));
    
#ifndef TRAJ
    for(int d = 0; d <= anzd; d++){
        
        double dichte = double(d)/anzd;
        cout << dichte;
#endif
#ifdef TRAJ
        double dichte = atof(argv[1]);
        int d = 1;
#endif
        loop(i,sl){
            s[i].a = 1.*rand()/RAND_MAX < dichte ? true : false; //wenn ne zufallszahl kleiner ist als dichte -> true, also wird mit dichte a
            if (s[i].a == true) {
                s[i].v = zufallszahl(maxv);// Straße wird zufällig mit Autos befüllt
                s[i].b = false;
				s[i].md = false;
            }
        }
    
        int anzautos = 0;
        loop(i,sl){
            if(s[i].a) anzautos++;
        }
        cout << "\t" << anzautos;
        //cout << "\n";
        ///  ############ hier passiert die schleife
        loop(k,eqz+mez){ //eqz+mez Update-Schritte
            update(k,d);
       /*     anzautos = 0;
            loop(i,sl){
                if(s[i].a) anzautos++;
            }
            cout << anzautos <<"\n"; */
#ifdef TRAJ
            if(k>=eqz){
                loop(x,sl){
                    strecke[k-eqz][x]= s[x].a == true ? true : false;
                }
            }
#endif
        }
     
#ifdef TRAJ
       string name = to_string(dichte) + ".txt";
        std::ofstream bild (name,std::ofstream::out);
        for(int k =0; k < mez; k++){
            for(int j=0 ;j<sl; j++){
                bild << strecke[k][j] << "\t";
            }
            bild << "\n";
        }
        bild.close();
        
     /*   anzautos =0;
        loop(i,sl){
            if(s[i].a) anzautos++;
        }
        
        cout << anzautos; */
#endif

#ifndef TRAJ
        flussdichte[d][0] = double(anzautos)/sl;
#endif
        
#ifdef EINS
        cout << "\t" << flussdichte[d][1] << "\n";
        
        flussdichte[d][1] = 60.*flussdichte[d][1]/mez;
#endif
        
#ifndef TRAJ
    }
    
    
#endif
    
#ifdef EINS
    std::ofstream ofs ("ns.txt", std::ofstream::out);
    for(int k=0; k <= anzd; k++){
    ofs << flussdichte[k][0] << "\t" << flussdichte[k][1] << "\n";
    }
#endif
    
#ifdef MITTEL
    std::ofstream ofs ("mittel.txt", std::ofstream::out);
    for(int k=0; k<= anzd; k++){
        ofs << flussdichte[k][0];
        for(int m=1; m<1+mez/60;m++){
            ofs << "\t" << flussdichte[k][m];
        }
        ofs << "\n";
        }
#endif
#ifndef TRAJ
    ofs.close();
#endif
	 return 0;
}

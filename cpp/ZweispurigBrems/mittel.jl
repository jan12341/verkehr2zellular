#!/usr/bin/julia

using CSV
using DelimitedFiles
using Statistics

m=readdlm("nsmittel.txt");
ml=readdlm("linksmittel.txt");
mr=readdlm("rechtsmittel.txt");

mittel = 20


m2=zeros(101,Int(100/mittel)+1)
m2l=zeros(101,Int(100/mittel)+1)
m2r=zeros(101,Int(100/mittel)+1)

for k in 1:101
    m2[k,1]=m[k,1]
    m2l[k,1]=ml[k,1]
    m2r[k,1]=mr[k,1]
end

for k in 1:101
    for j in 1:Int(100/mittel)
    summe=0
    summel=0
    summer=0
    for i in mittel*j+2-mittel:mittel*j+1
        summe = summe + m[k,i]
        summel = summel + ml[k,i]
        summer = summer + mr[k,i]
    end
    m2[k,j+1]=summe/mittel
    m2l[k,j+1]=summel/mittel
    m2r[k,j+1]=summer/mittel
    end
end

writedlm("mw.txt",m2,"\t")
writedlm("mwr.txt",m2r,"\t")
writedlm("mwl.txt",m2l,"\t")



#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>

#define loop(x, n) for(int x = 0; x < n; ++ x)

using namespace std;

// Definieren einiger Konstanten
#define sl 1000
#define maxv 5
//#define trw 0.3
#define trwanf 0.5
#define trwb 0.94
#define trwd 0.1
#define eqz 2000
#define mez 6000 //muss vielfaches von 60 sein, am besten so, dass mez / (60*20) eine ganze Zahl ist
#define anzd 100 // mw.jl arbeitet mit 100 dichten
#define dsafe 4 //Sicherheitsabstand
#define h 7 //Blickweite

// Deklaierung und Erzeugung des Structs für alle Autoinfos und des Gitters
struct autoinfos {
    bool a;
    int v;
    bool b;
    bool md;
}; 

struct autoinfos s[sl][2];  // array aus den structs, zweispurig

struct autoinfos  leer = {false, 0, false, false};

#ifdef EINS
double flussdichte [anzd+1][2];
double dichtelinks [anzd+1][2];
double dichterechts [anzd+1][2];
#endif

#ifdef MITTEL
double flussdichte [anzd+1][1+mez/60];
double dichtelinks [anzd+1][1+mez/60];
double dichterechts[anzd+1][1+mez/60];
#endif

#ifdef TRAJ
bool strecke [mez][sl];
bool streckelinks [mez][sl];
#endif

//Funktion zur zufälligen Besetzung
bool randomBool()
{
    return (rand() % 2) == 1;
}

//Funktion für Trödeln
double trödeln()
{
    return ((double) rand()/ RAND_MAX);
}

//Funktion für Anfangsgeschwindigkeit
int zufallszahl(int obere_grenze)
{
    return rand()/double(RAND_MAX)*(obere_grenze+1);
}

//Funktionen, die für Bremslichtmodell benötigt werden


//Funktion die für alle Autos die Updates macht
void update(int zeit,int dichtezahl){
  //  loop(i,sl) cout << "@0" << s[i][0].a << "\t";

    // nach links fahren
        //zuerst müssen wir auf der rechten spur alle effektiven abstände kennen
        int vanthier[sl][2]; //Beschreibungen siehe Berechnung
        int daddhier[sl][2];
        int deffhier[sl][2];
        int lückehier[sl];
        loop(g,sl){ //gehe alle autos durch
            loop(i,sl){ //effektiver Abstand auf der rechten Spur
                if (s[i][0].a==true) {
                    int j = (i+1) % sl; //+1 weil wir erst die naechste Zelle nehmen
                    int lücke = 0;
                    while (s[j][0].a == false && j != i){ //wir gucken nicht mehr nur so weit wie unsere Geschwindigkeit, weil wir die Position des Vordermannes benötigen
                        lücke++;
                        j = ( j + 1) % sl; //verwende periodische RB
                    }
                    vanthier[i][0] = lücke < s[i][0].v ? lücke - 1 : s[i][0].v - 1; //Antizipierte Geschwindigkeit des Vordermannes, siehe Skript
                    lückehier[i]=lücke;
                }
            }
            loop(i,sl){
                if(s[i][0].a==true){
                    int j=(i+1+lückehier[i])&sl;
                    daddhier[i][0] = vanthier[j][0] - dsafe > 0 ? vanthier[j][0] - dsafe : 0; //Abstand der zum Abstand addiert wird um den effektiven Abstand zu erhalten
                    deffhier[i][0] = lückehier[i] + daddhier[i][0]; //Effektiver Abstand zum Vordermann
                }
            }
            
            loop(i,sl) lückehier[i]=0;
            
            if(s[g][0].a == true){ //nur wenn da ein auto ist
                if(deffhier[g][0] < s[g][0].v && s[g][1].a == false){ // wenn der effektive Abstand rechts zum vordermann kleiner ist als die geschwindigkeit und auf der linken spur kein auto ist
                    s[g][1] = s[g][0]; //kopiere das auto rüber
                    loop(l,sl){ // berechne jetzt alle eff abstände auf der linken spur
                        if (s[l][1].a==true) {  // berechne auf der linken spur für alle autos jetzt den effektiven abstand
                            int j = (l+1) % sl; //+1 weil wir erst die naechste Zelle nehmen
                            int lücke = 0;
                            while (s[j][1].a == false && j != l){
                                lücke++;
                                j = ( j + 1) % sl; //verwende periodische RB
                            }
                            vanthier[l][1] = lücke < s[l][1].v ? lücke - 1 : s[l][1].v - 1; //Antizipierte Geschwindigkeit des Vordermannes, siehe Skript
                            lückehier[l]=lücke;
                        }
                    }
                    loop(l,sl){
                        if(s[l][1].a==true){
                            int j=(l+1+lückehier[l])%sl;
                            daddhier[l][1] = vanthier[j][1] - dsafe > 0 ? vanthier[j][1] - dsafe : 0; //Abstand der zum Abstand addiert wird um den effektiven Abstand zu erhalten
                            deffhier[l][1] = lückehier[l] + daddhier[l][1]; //Effektiver Abstand zum Vordermann
                        }
                    }
                    // jetzt gucken, ob das auto nach links wechselt
                    int poshinten = (g-1)%sl;
                    while (s[poshinten][1].a == false && poshinten != g) poshinten = (poshinten -1) % sl; //wir brauchen die position des hintermanns
                    if(deffhier[g][0] < deffhier[g][1] && deffhier[poshinten][1] > s[poshinten][1].v) s[g][0]=leer;
                    else s[g][1] = leer;  //da wir periodische RB haben müssen wir, falls das auto wechselt die eff Abstände neu ausrechnen
                }
            }
        }
//    loop(i,sl) cout << "@links" << s[i][0].a << "\t";
   // cout << "@links \n";
        
        //nach rechts fahren
        int vanthier2[sl];
        int daddhier2[sl];
        int deffhier2[sl];
        int lückehier2[sl];
        loop(g,sl){
            if(s[g][0].a==false && s[g][1].a == true){ //falls da kein Auto ist und wir überhaupt eins betrachten können
                s[g][0] = s[g][1]; // kopiere das auto rüber
                loop(i,sl){ //effektiver Abstand auf der rechten Spur
                    if (s[i][0].a==true) {
                        int j = (i+1) % sl; //+1 weil wir erst die naechste Zelle nehmen
                        int lücke = 0;
                        while (s[j][0].a == false && j != i){ //wir gucken nicht mehr nur so weit wie unsere Geschwindigkeit, weil wir die Position des Vordermannes benötigen
                            lücke++;
                            j = ( j + 1) % sl; //verwende periodische RB
                        }
                        vanthier2[i] = lücke < s[i][0].v ? lücke - 1 : s[i][0].v - 1; //Antizipierte Geschwindigkeit des Vordermannes, siehe Skript
                        lückehier2[i]=lücke;
                    }
                }
                loop(i,sl){
                    if(s[i][0].a==true){
                        int j=(i+lückehier2[i]+1)%sl;
                        daddhier2[i] = vanthier2[j] - dsafe > 0 ? vanthier2[j] - dsafe : 0; //Abstand der zum Abstand addiert wird um den effektiven Abstand zu erhalten
                        deffhier2[i] = lückehier2[i]+ daddhier2[i]; //Effektiver Abstand zum Vordermann
                    }
                }
                int poshinten = (g-1)%sl;
                while (s[poshinten][0].a == false && poshinten != g) poshinten = (poshinten -1) % sl; //wir brauchen die position des hintermanns
                if(deffhier2[g] > s[g][0].v && deffhier2[poshinten] > s[poshinten][0].v) s[g][1] = leer;
                else s[g][0] = leer;
            }
        }
    //cout << "@rechts \n";
   // loop(i,sl) cout << "@rechts" << s[i][0].a << "\t";
    
    ///////// die müssen auch alle zweispurig werden
    int vant[sl][2]; //Beschreibungen siehe Berechnung
    int dadd[sl][2];
    int deff[sl][2];
    double dtime[sl][2];
    int dint[sl][2];
    int bm[sl][2];
    double trw[sl][2];
    int lücke[sl][2];
    // Schritt 0, wichtige Parameter, die im Folgenden benötigt werden, berechnen
    loop(k,2){  //berechne alle Parameter für beide Spuren unabhängig
        loop(i,sl){
            if (s[i][k].a==true) {  //wir berechnen das alles für jeden platz, ob auto oder nicht. wir brauchen später den effektiven abstand und für den ist egal, ob da ein auto ist oder nicht
                int j = (i+1) % sl; //+1 weil wir erst die naechste Zelle nehmen
                int lucke = 0;
                while (s[j][k].a == false && j != i){ //wir gucken nicht mehr nur so weit wie unsere Geschwindigkeit, weil wir die Position des Vordermannes benötigen
                    lucke++;
                    j = ( j + 1) % sl; //verwende periodische RB
                }
                lücke[i][k]=lucke;
                vant[i][k] = lucke < s[i][k].v ? lucke - 1 : s[i][k].v - 1; //Antizipierte Geschwindigkeit des Vordermannes, siehe Skript
            }
        }
        loop(i,sl){
            if(s[i][k].a==true){
                int j=(i+lücke[i][k]+1)%sl;
                dadd[i][k] = vant[j][k] - dsafe > 0 ? vant[j][k] - dsafe : 0; //Abstand der zum Abstand addiert wird um den effektiven Abstand zu erhalten
                deff[i][k] = lücke[i][k] + dadd[i][k]; //Effektiver Abstand zum Vordermann
                dtime[i][k] = double(lücke[i][k]) /s[i][k].v; //Zeitlicher Abstand zum Vordermann
                dint[i][k] = s[i][k].v < h ? s[i][k].v : h; //Interaktionshorizont
                bm[i][k] = s[j][k].b == true ? 1: 0;
                if (s[i][k].v == 0) {
                    trw[i][k] = trwanf;
                } else if(s[j][k].b == true && dtime[i][k] < dint[i][k]) {
                    trw[i][k] = trwb;
                } else {
                    trw[i][k] = trwd;
                }
            }
        }
    }
    
    struct autoinfos autozwischen[sl][2];
    loop(q,2){
        loop(i,sl) {
            autozwischen[i][q] = leer;
        }
    }
    
	loop(k,2){  // die Schritte laufen für die Spuren unabhängig ab
        // Schritt 1, Beschleunigen, falls noch nicht Maximalgeschwindigkeit
    
        loop(i,sl){
            if (s[i][k].a == true) {
                if (s[i][k].b == true || (bm[i][k] == 1 && dtime[i][k] < dint[i][k]) ) {
                    s[i][k].v = s[i][k].v ;
                } else {
                    s[i][k].v = s[i][k].v < maxv ? s[i][k].v + 1 : maxv; // alle ternären Operatoren durch min(a,b) ersetzen
                }
              s[i][k].b = false;
            }
        }
//loop(i,sl) cout << "@1" << s[i][0].a << "\t";
        // Schritt 2, falls Lücke kleiner als Geschwindigkeit -> Geschwindigkeit = Lücke, sonst nichts
        loop(i,sl){
            //int vschritt[sl] = 0; //Wir benötigen eine Geschwindigkeit zum zwischenspeichern in diesem Schritt
            if (s[i][k].a==true) {
                int vschritt = s[i][k].v < deff[i][k] ? s[i][k].v : deff[i][k]; //Wir benötigen eine Geschwindigkeit zum zwischenspeichern in diesem Schritt
                if (vschritt < s[i][k].v) {s[i][k].b = true; }
                s[i][k].v = vschritt;
            }
        }
//loop(i,sl) cout << "@2" << s[i][0].a << "\t";
        // Schritt 3, Trödeln: Verringerung mit Wahrscheinlichkeit p, wenn nicht schon steht
        loop(i,sl){
            if (s[i][k].a == true && s[i][k].v != 0){
                int vschritt = trödeln() < trw[i][k] ? s[i][k].v -1 : s[i][k].v; //Wir benötigen eine Geschwindigkeit zum zwischenspeichern in diesem Schritt
                if (vschritt < s[i][k].v && trw[i][k] == trwb) {s[i][k].b = true; }
                s[i][k].v = vschritt;
            }
        }
//loop(i,sl) cout << "@3" << s[i][0].a << "\t";
        // Schritt 4, alle mit Geschwindigkeit nach vorne
        
        
        
        loop(i,sl){
            if (s[i][k].a == true && s[i][k].md == false){
                int neuerplatz = (i + s[i][k].v) % sl; //periodische Randbedingungen
               
                if (zeit > eqz && neuerplatz < i){ // wenn equilibriert und auto rechts raus
    #ifdef MITTEL
                     int step = zeit / 60 + 1; // gibt die größte zahl die durch 60 geht
                     if(k==0) dichterechts[dichtezahl][step]++; //fluss einen hoch
                     if(k==1) dichtelinks[dichtezahl][step]++;
                    flussdichte[dichtezahl][step]++; //fluss einen hoch
                    //cout << "@@";
    #endif
    #ifdef EINS
                    flussdichte[dichtezahl][1]++;
                    if(k==0) dichterechts[dichtezahl][1]++;
                    if(k==1) dichtelinks[dichtezahl][1]++;
    #endif
                }
                
                autozwischen[neuerplatz][k] = s[i][k];
                s[i][k].md = true;
               // if (s[neuerplatz][k].v > 0) s[i][k] = leer;
        
            }
        }
        loop(i,sl){
            s[i][k]=autozwischen[i][k];
            autozwischen[i][k]=leer;
        }
        loop(i,sl) s[i][k].md = false;
    }
   // loop(i,sl) cout << "@4" << s[i][0].a << "\t";
}



int main(int argc, char* argv[]){

    srand(time(0));
    
#ifndef TRAJ
    for(int d = 0; d <= anzd; d++){
        
        double dichte = double(d)/anzd;
        cout << dichte;
#endif
#ifdef TRAJ
        double dichte = atof(argv[1]);
        int d = 1;
#endif
        loop(k,2){
            loop(i,sl){
                s[i][k]=leer;
            }
        }
        loop(k,2){  //beide Spuren werden anfangs besetzt
            loop(i,sl){
                s[i][k].a = 1.*rand()/RAND_MAX < dichte ? true : false; //wenn ne zufallszahl kleiner ist als dichte -> true, also wird mit dichte a
                if (s[i][k].a == true) {
                    s[i][k].v = zufallszahl(maxv);// Straße wird zufällig mit Autos befüllt
                    s[i][k].b = false;
                    s[i][k].md = false;
                }
            }
        }
        //cout << "besetzt \n";
      //  loop(i,sl) cout <<"@init" << s[i][0].a << "\t";
        int anzautos = 0;
        loop(k,2){
            loop(i,sl){
                if(s[i][k].a) anzautos++;
            }
        }
        //cout << "\t" << anzautos;
        ///  ############ hier passiert die schleife
        loop(u,eqz+mez){ //eqz+mez Update-Schritte
            update(u,d);
#ifdef TRAJ
            if(u>=eqz){
                loop(x,sl){
                    strecke[u-eqz][x] = s[x][0].a == true ? true : false; //rechts
                    streckelinks[u-eqz][x] = s[x][1].a == true ? true : false;
                }
               /* loop(i,sl){
                    cout << s[i][0].a << "\t";
                } */
            }
            //if(u >=eqz)cout << "\n";
#endif
        }
     
#ifdef TRAJ
        string name = to_string(dichte) + ".txt";
        string namelinks = to_string(dichte) + "links.txt";
        std::ofstream bild (name,std::ofstream::out);
        std::ofstream bildlinks (namelinks,std::ofstream::out);
        for(int k =0; k < mez; k++){
            for(int j=0 ;j<sl; j++){
                bild << strecke[k][j] << "\t";
                bildlinks << streckelinks[k][j] << "\t";
            }
            bild << "\n";
            bildlinks << "\n";
        }
        bild.close();
        bildlinks.close();
#endif

#ifndef TRAJ
        flussdichte[d][0] = dichterechts[d][0] = dichtelinks[d][0] = double(anzautos)/(2*sl);
#endif
        
#ifdef EINS
        cout << "\t" << flussdichte[d][1] << "\n";
        
        flussdichte[d][1] = 60.*flussdichte[d][1]/mez;
        dichterechts[d][1] = 60.*dichterechts[d][1]/mez;
        dichtelinks[d][1] = 60.*dichtelinks[d][1]/mez;
#endif
        
#ifndef TRAJ
    }
    
#endif
    
#ifdef EINS
    
    std::ofstream ofs ("ns.txt", std::ofstream::out);
    std::ofstream links("links.txt",std::ofstream::out);
    std::ofstream rechts("rechts.txt",std::ofstream::out);

    for(int k=0; k <= anzd; k++){
    ofs << flussdichte[k][0] << "\t" << flussdichte[k][1] << "\n";
    links << flussdichte[k][0] << "\t" << dichtelinks[k][1] << "\n";
    rechts << flussdichte[k][0] << "\t" << dichterechts[k][1] << "\n";
    }
#endif
    
#ifdef MITTEL
    std::ofstream ofs ("nsmittel.txt", std::ofstream::out);
    std::ofstream links("linksmittel.txt",std::ofstream::out);
    std::ofstream rechts("rechtsmittel.txt",std::ofstream::out);

    
    for(int k=0; k<= anzd; k++){
        ofs << flussdichte[k][0];
        links << dichtelinks[k][0];
        rechts << dichterechts[k][0];
        for(int m=1; m<1+mez/60;m++){
            ofs << "\t" << flussdichte[k][m];
            links << "\t" << dichtelinks[k][m];
            rechts << "\t" << dichterechts[k][m];
        }
        ofs << "\n";
        links << "\n";
        rechts << "\n";
        }
#endif
#ifndef TRAJ
    ofs.close();
    links.close();
    rechts.close();
#endif
	 return 0;
}

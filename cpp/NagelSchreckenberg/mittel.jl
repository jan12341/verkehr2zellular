#!/usr/bin/julia

using CSV
using DelimitedFiles
using Statistics

m=readdlm("mittel.txt");

mittel = 20


m2=zeros(101,Int(100/mittel)+1)
for k in 1:101
    m2[k,1]=m[k,1]
end

for k in 1:101
    for j in 1:Int(100/mittel)
    summe=0
    for i in mittel*j+2-mittel:mittel*j+1
        summe = summe + m[k,i]
    end
    m2[k,j+1]=summe/mittel
    end
end

writedlm("mw.txt",m2,"\t")

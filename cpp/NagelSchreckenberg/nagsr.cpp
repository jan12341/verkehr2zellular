#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>

#define loop(x, n) for(int x = 0; x < n; ++ x)

using namespace std;

// Definieren einiger Konstanten
#define sl 1000
#define maxv 5
#define trw 0.3
#define eqz 200
#define mez 6000 //muss vielfaches von 60 sein
#define anzd 100

// Deklaierung und Erzeugung des Structs für alle Autoinfos und des Gitters
struct autoinfos {
    bool a;
    int v;
    bool b;
    bool md;
}; 

struct autoinfos s[sl];  // array aus den structs

struct autoinfos  leer = {false, 0, false, false};
#ifdef EINS
double flussdichte [anzd+1][2];
#endif

#ifdef MITTEL
double flussdichte [anzd+1][1+mez/60];
#endif

#ifdef TRAJ
int strecke [mez][sl];
#endif

//Funktion zur zufälligen Besetzung
bool randomBool()
{
    return (rand() % 2) == 1;
}

//Funktion für Trödeln
double trödeln()
{
    return ((double) rand()/ RAND_MAX);
}

//Funktion für Anfangsgeschwindigkeit
int zufallszahl(int obere_grenze)
{
    return rand()/double(RAND_MAX)*(obere_grenze+1);
}

//Funktion die für alle Autos die Updates macht
void update(int zeit,int dichtezahl){
    
    // Schritt 1, Beschleunigen, falls noch nicht Maximalgeschwindigkeit
    loop(i,sl){
        if (s[i].a == true) { s[i].v = s[i].v < maxv ? s[i].v + 1 : maxv; } // alle ternären Operatoren durch min(a,b) ersetzen
    }

    // Schritt 2, falls Lücke kleiner als Geschwindigkeit -> Geschwindigkeit = Lücke, sonst nichts
    loop(i,sl){
        if (s[i].a==true) {
            int j = (i+1) % sl; //+1 weil wir erst die naechste Zelle nehmen
            int lücke = 0;
            while (s[j].a == false && lücke < s[i].v){ //wir gucken nicht weiter als unsere Geschwindigkeit
                lücke++;
                j = ( j + 1) % sl; //verwende periodische RB
            }
            s[i].v = s[i].v < lücke ? s[i].v : lücke;
        }
    }
    
    // Schritt 3, Trödeln: Verringerung mit Wahrscheinlichkeit p, wenn nicht schon steht
    loop(i,sl){
        if (s[i].a == true && s[i].v != 0){
            s[i].v = trödeln() < trw ? s[i].v -1 : s[i].v;
        }
    }

    // Schritt 4, alle mit Geschwindigkeit nach vorne
    loop(i,sl){
        if (s[i].a == true && s[i].md == false){
            int neuerplatz = (i + s[i].v) % sl; //periodische Randbedingungen
           
            if (zeit > eqz && neuerplatz < i){ // wenn equilibriert und auto rechts raus
#ifdef MITTEL
                int step = zeit / 60 + 1; // gibt die größte zahl die durch 60 geht
                flussdichte[dichtezahl][step]++; //fluss einen hoch
#endif
#ifdef EINS
                flussdichte[dichtezahl][1]++;
#endif
            }
            
			s[neuerplatz] = s[i];
            s[neuerplatz].md = true;
			if (s[neuerplatz].v > 0) s[i] = leer;
	
        }
    }
    
    loop(i,sl) s[i].md = false;
}



int main(int argc, char* argv[]){

    srand(time(0));
    
#ifndef TRAJ
    for(int d = 0; d <= anzd; d++){
        
        double dichte = double(d)/anzd;
        cout << dichte;
#endif
#ifdef TRAJ
        double dichte = atof(argv[1]);
        int d = 1;
#endif
        loop(i,sl){
            s[i].a = 1.*rand()/RAND_MAX < dichte ? true : false; //wenn ne zufallszahl kleiner ist als dichte -> true, also wird mit dichte a
            if (s[i].a == true) s[i].v = zufallszahl(maxv);// Straße wird zufällig mit Autos befüllt
        }
    
        int anzautos = 0;
        loop(i,sl){
            if(s[i].a) anzautos++;
        }
        cout << "\t" << anzautos;
        ///  ############ hier passiert die schleife
        loop(k,eqz+mez){ //eqz+mez Update-Schritte
            update(k,d);
#ifdef TRAJ
            if(k>=eqz){
                loop(x,sl){
                    strecke[k-eqz][x]= s[x].a == true ? true : false;
                }
            }
#endif
        }
      
#ifdef TRAJ
       string name = to_string(dichte) + ".txt";
        std::ofstream bild (name,std::ofstream::out);
        for(int k =0; k < mez; k++){
            for(int j=0 ;j<sl; j++){
                bild << strecke[k][j] << "\t";
            }
            bild << "\n";
        }
        bild.close();
#endif
    
        
#ifndef TRAJ
        flussdichte[d][0] = double(anzautos)/sl;
#endif
        
#ifdef EINS
        cout << "\t" << flussdichte[d][1] << "\n";
        
        
        flussdichte[d][1] = 60.*flussdichte[d][1]/mez;
#endif
#ifndef TRAJ
    }
    
#endif
    
#ifdef EINS
    std::ofstream ofs ("ns.txt", std::ofstream::out);
    for(int k=0; k <= anzd; k++){
    ofs << flussdichte[k][0] << "\t" << flussdichte[k][1] << "\n";
    }
#endif
    
#ifdef MITTEL
    std::ofstream ofs ("mittel.txt", std::ofstream::out);
   for(int k=0; k<= anzd; k++){
        ofs << flussdichte[k][0];
        for(int m=1; m<1+mez/60;m++){
            ofs << "\t" << flussdichte[k][m];
        }
        ofs << "\n";
        }
#endif
#ifndef TRAJ
    ofs.close();
#endif
	 return 0;
}

#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>

#define loop(x, n) for(int x = 0; x < n; ++ x)

using namespace std;

// Definieren einiger Konstanten
#define sl 1000
#define maxv 5
#define trw 0.3
#define eqz 1000
#define mez 6000 //muss vielfaches von 60 sein
#define anzd 100

// Deklaierung und Erzeugung des Structs für alle Autoinfos und des Gitters
struct autoinfos {
    bool a;
    int v;
    bool b;
    bool md;
}; 

struct autoinfos s[sl][2];  // array aus den structs  ---> zweispurig  [0] ist rechts spur, [1] die linke

struct autoinfos  leer = {false, 0, false, false};

#ifdef EINS
double flussdichte [anzd+1][2];
double dichtelinks [anzd+1][2];
double dichterechts [anzd+1][2];
#endif

#ifdef MITTEL
double flussdichte [anzd+1][1+mez/60];
double dichtelinks [anzd+1][1+mez/60];
double dichterechts[anzd+1][1+mez/60];

#endif

#ifdef TRAJ
int strecke [mez][sl];
int streckelinks [mez][sl];

#endif

//Funktion zur zufälligen Besetzung
bool randomBool()
{
    return (rand() % 2) == 1;
}

//Funktion für Trödeln
double trödeln()
{
    return ((double) rand()/ RAND_MAX);
}

//Funktion für Anfangsgeschwindigkeit
int zufallszahl(int obere_grenze)
{
    return rand()/double(RAND_MAX)*(obere_grenze+1);
}

//Funktion die für alle Autos die Updates macht
void update(int zeit,int dichtezahl){
    //Schritt 0  Spurwechsel?
    
    // man fährt nur nach links, wenn man links schneller fahren kann als auf der eigenen Spur -> der Abstand zum Vordermann muss kleiner sein als v+1 und links der Abstand muss dann mindestens v+1 sein
    // zusätzlich muss der Abstand links nach hinten größer als v+1 des folgenden Autos sein
    
    
    // nach rechts fahren?
    
    loop(i,sl){
        if(s[i][1].a == true){
            int vorner = (i+1) % sl; //+1 weil wir erst die naechste Zelle nehmen
            int lücke = 0;
            while (s[vorner][0].a == false && vorner != i){ //wir bestimmen die lücke nach vorne auf der rechten
                lücke++;
                vorner = ( vorner + 1) % sl;
            }
            int hintenr = (i-1) %sl;
            int lückehinten = 0;
            while(s[hintenr][0].a == false && hintenr != i){ // wir bestimmen die lücke nach hinten auf der rechten spur
                lückehinten++;
                hintenr = (hintenr-1)%sl;
            }
            if(s[i][1].v <= lücke && s[hintenr][0].v <= lückehinten && s[i][0].a == false){  //falls wir rechts nicht abbremsen müssen und der hinter uns nicht abbremsen muss, zusätzlich gucken ob zelle frei
                s[i][0] = s[i][1];
                s[i][1] = leer;
            }
        }
    }
    
    
    // nach links fahren?
    loop(i,sl){
        if ( s[i][0].a == true){
            int vorner = (i+1) % sl; //+1 weil wir erst die naechste Zelle nehmen
            int lücke = 0;
            while (s[vorner][0].a == false && vorner != i){ //wir bestimmen die lücke nach vorne auf unserer spur
                lücke++;
                vorner = ( vorner + 1) % sl;
            }
            int vornel = (i+1) % sl;
            int lückelinks = 0;
            while(s[vornel][1].a == false && vornel != i){ // wir bestimmen die lücke nach vorne auf der linken spur
                lückelinks++;
                vornel = (vornel+1)%sl;
            }
            int hintenl = (i-1) %sl;
            int lückehinten = 0;
            while(s[hintenl][1].a == false && hintenl != i){ // wir bestimmen die lücke nach hinten auf der linken spur
                lückehinten++;
                hintenl = (hintenl-1)%sl;
            }
            if(s[i][0].v > lücke && s[i][0].v <= lückelinks && s[hintenl][1].v <= lückehinten && s[i][1].a==false){  //zusätzlich gucken, ob die zelle links frei ist
                s[i][1] = s[i][0];
                s[i][0] = leer;
            }
        }
    }
    
    
    // die restlichen Schritte laufen für die beiden spuren unabhängig ab
    
    loop(k,2){
        // Schritt 1, Beschleunigen, falls noch nicht Maximalgeschwindigkeit
        loop(i,sl){
            if (s[i][k].a == true) { s[i][k].v = s[i][k].v < maxv ? s[i][k].v + 1 : maxv; } // alle ternären Operatoren durch min(a,b) ersetzen
        }

        // Schritt 2, falls Lücke kleiner als Geschwindigkeit -> Geschwindigkeit = Lücke, sonst nichts
        loop(i,sl){
            if (s[i][k].a==true) {
                int j = (i+1) % sl; //+1 weil wir erst die naechste Zelle nehmen
                int lücke = 0;
                while (s[j][k].a == false && lücke < s[i][k].v){ //wir gucken nicht weiter als unsere Geschwindigkeit
                    lücke++;
                    j = ( j + 1) % sl; //verwende periodische RB
                }
                s[i][k].v = s[i][k].v < lücke ? s[i][k].v : lücke;
            }
        }
        
        // Schritt 3, Trödeln: Verringerung mit Wahrscheinlichkeit p, wenn nicht schon steht
        loop(i,sl){
            if (s[i][k].a == true && s[i][k].v != 0){
                s[i][k].v = trödeln() < trw ? s[i][k].v -1 : s[i][k].v;
            }
        }

        // Schritt 4, alle mit Geschwindigkeit nach vorne
        loop(i,sl){
            if (s[i][k].a == true && s[i][k].md == false){
                int neuerplatz = (i + s[i][k].v) % sl; //periodische Randbedingungen
               
                if (zeit > eqz && neuerplatz < i){ // wenn equilibriert und auto rechts raus
    #ifdef MITTEL
                    int step = zeit / 60 + 1; // gibt die größte zahl die durch 60 geht
                    flussdichte[dichtezahl][step]++; //fluss einen hoch
                    if(k==0) dichterechts[dichtezahl][step]++;
                    if(k==1) dichtelinks[dichtezahl][step]++;
    #endif
    #ifdef EINS
                    flussdichte[dichtezahl][1]++;
                    if(k==0) dichterechts[dichtezahl][1]++;
                    if(k==1) dichtelinks[dichtezahl][1]++;
                    
    #endif
                }
                
                s[neuerplatz][k] = s[i][k];
                s[neuerplatz][k].md = true;
                if (s[neuerplatz][k].v > 0) s[i][k] = leer;
        
            }
        }
        
        loop(i,sl) s[i][k].md = false;
    }
}


int main(int argc, char* argv[]){

    srand(time(0));
    
#ifndef TRAJ
    for(int d = 0; d <= anzd; d++){
        
        double dichte = double(d)/anzd;
        cout << dichte;
#endif
#ifdef TRAJ
        double dichte = atof(argv[1]);
        int d = 1;
#endif
        loop(k,2){
            loop(i,sl){
                s[i][k].a = 1.*rand()/RAND_MAX < dichte ? true : false; //wenn ne zufallszahl kleiner ist als dichte -> true, also wird mit dichte a
                if (s[i][k].a == true) s[i][k].v = zufallszahl(maxv);// Straße wird zufällig mit Autos befüllt, beide Spuren gleich
            }
        }
        int anzautos = 0;
        loop(k,2){
            loop(i,sl){
                if(s[i][0].a) anzautos++;
            }
        }
        cout << "\t" << anzautos;
        ///  ############ hier passiert die schleife
        loop(k,eqz+mez){ //eqz+mez Update-Schritte
            update(k,d);
#ifdef TRAJ
            if(k>=eqz){
                loop(x,sl){
                    strecke[k-eqz][x]= s[x][0].a == true ? true : false;
                    streckelinks[k-eqz][x] = s[x][1].a == true ? true : false;
                }
            }
#endif
        }
      
#ifdef TRAJ
        string name = to_string(dichte) + ".txt";
        string namelinks = to_string(dichte) + "links.txt";
        std::ofstream bild (name,std::ofstream::out);
        std::ofstream bildlinks (namelinks,std::ofstream::out);
        for(int k =0; k < mez; k++){
            for(int j=0 ;j<sl; j++){
                bild << strecke[k][j] << "\t";
                bildlinks << streckelinks[k][j] << "\t";
            }
            bild << "\n";
            bildlinks << "\n";
        }
        bild.close();
        bildlinks.close();
#endif
    
        
#ifndef TRAJ
        flussdichte[d][0] =  double(anzautos)/(2*sl);  //Es werden beide Spuren besetzt
        dichterechts[d][0] = double(anzautos)/(2*sl);
        dichtelinks[d][0] = double(anzautos)/(2*sl);
#endif
        
#ifdef EINS
        cout << "\t" << flussdichte[d][1] << "\n";
        
        
        flussdichte[d][1] = 60.*flussdichte[d][1]/mez;
        dichterechts[d][1] = 60.*dichterechts[d][1]/mez;
        dichtelinks[d][1] = 60.*dichtelinks[d][1]/mez;
#endif
#ifndef TRAJ
    }
   // std::ofstream ofs ("ns.txt", std::ofstream::out);
   // std::ofstream links("links.txt",std::ofstream::out);
   // std::ofstream rechts("rechts.txt",std::ofstream::out);
    
#endif
    
#ifdef EINS
    std::ofstream ofs ("ns.txt", std::ofstream::out);
    for(int k=0; k <= anzd; k++){
    ofs << flussdichte[k][0] << "\t" << flussdichte[k][1] << "\n";
    }
    ofs.close();
    
    std::ofstream links ("links.txt", std::ofstream::out);
    for(int k=0; k <= anzd; k++){
        links << flussdichte[k][0] << "\t" << dichtelinks[k][1] << "\n";
    }
    links.close();
    
    std::ofstream rechts ("rechts.txt", std::ofstream::out);
    for(int k=0; k <= anzd; k++){
        rechts << flussdichte[k][0] << "\t" << dichterechts[k][1] << "\n";
    }
    rechts.close();
    
   // cout << dichterechts[0][0] << "\t" << flussdichte[0][0];
#endif
    
#ifdef MITTEL
     std::ofstream ofs ("nsmittel.txt", std::ofstream::out);
     std::ofstream links("linksmittel.txt",std::ofstream::out);
     std::ofstream rechts("rechtsmittel.txt",std::ofstream::out);
   for(int k=0; k<= anzd; k++){
        ofs << flussdichte[k][0];
       links << dichtelinks[k][0];
       rechts << dichterechts[k][0];
        for(int m=1; m<1+mez/60;m++){
            ofs << "\t" << flussdichte[k][m];
            links << "\t" << dichtelinks[k][m];
            rechts << "\t" << dichterechts[k][m];
        }
        ofs << "\n";
        links << "\n";
        rechts << "\n";
        }
    ofs.close();
    links.close();
    rechts.close();
#endif
	 return 0;
}

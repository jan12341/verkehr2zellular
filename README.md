# Verkehr2Zellular

Zellularautomat für das Verkehr 2 Projekt.
Es gibt sowohl das Nagel Schreckenberg als auch das Bremslichtmodell. Man kann das Fundamentaldiagramm berechnen indem für die ganze Zeit gemessen und dann auf 60s runtergerechnet wird, andererseits kann immer für eine Minute ausgegegeben werden, sodass nachher gemittelt werden kann. Zusätzlich kann für gegebene Dichte die Trajektorie 
ausgegeben werden.

Es gibt zum Plotten der Trajektorie und zur MW Berechnung Julia scripte. 

Es gibt gute Einführungen zu git im Internet, die Git Seite der Uni hat auch ne Dokumentation, sonst einfach fragen.
Laut dem Pferd ist gitfürwindows gut geeignet, da es eine Bash mitbringt, mit der ssh sowie ssh keygen verwendet werden können, sodass beim pullen/pushen kein PW eingegeben werden muss. Es meinte, man muss
noch ein paar Sachen einstellen, daher sollten wir das wohl zusammen installieren.

git arbeitet nicht mit den Dateien selbst sondern mit Änderungen

git $1

pull: zieht alle Änderungen vom Server, falls es welche gibt
add: DATEI added die gemachten Änderungen
    . addet die Änderungen aller Dateien
commit -m "kommentar" fügt die Änderungen "richtig" hinzu mit dem Kommentar
push schickt alles an den Server
diff DATEI zeigt Differenzen zur aktuellen Version in git an
status zeigt an, ob es was zu adden/commiten gibt oder so

